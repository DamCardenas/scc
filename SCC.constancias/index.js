const electron = require("electron");
const ipcMain = require("electron").ipcMain;
const appController=require("./app.controller");
const pdf_viewer=require('electron-pdf-window');
const BrowserWindow = electron.BrowserWindow;
const app = electron.app;


// Controladores
const actividadController=require('./controladores/actividad.controller');
const alumnoController=require('./controladores/alumno.controller');
const constanciaController=require('./controladores/constancia.controller');
const carreraController=require('./controladores/carrera.controller');
const configuracionController=require('./controladores/configuracion.controller');
const sccUtil=require('./controladores/scc.util');
//const configController=require('./config.controller');


let dbPath=`${app.getPath('userData')}/Configuracion/local_db.db`;
let dbDefaulSchemaPath=`${app.getPath('userData')}/Configuracion/default_schema.sql`;


let win;
let initstate=false;
var openWindow = (filename, _width, _height, _minWidth = _width, _minHeight = _height, _transparent = false, _frame = true, _aot = false, _max = false) => {
    win = new BrowserWindow({ width: _width, height: _height, minWidth: _minWidth, minHeight: _minHeight, transparent: _transparent, frame: _frame, alwaysOnTop: _aot, webPreferences: { nodeIntegration: true,plugins: true }, show: false });
    if (_max) win.maximize();
    if (debug) win.webContents.openDevTools();
    win.removeMenu();
    win.loadURL(`file://${__dirname}/` + filename + `.html`);
    let ses = win.webContents.session;
    ses.clearCache();
    win.once('ready-to-show', () => {
        win.show();
    })
}


ipcMain.on('obtener-configuracion-asincrono',(event,args)=>{
    let ccon=new configuracionController(dbPath);
    event.reply('r-obtener-configuracion-asincrono',ccon.obtenerConfiguracion());
})





// FUNCIONES BASICAS DE CONTROLADOR

// IPCMAIN
// Inicializar ventana de carga
ipcMain.on('start',(event,args)=>{
    openWindow("./vistas/alumnos", 1024, 680, 500, 300, false, false)
    event.returnValue=null
});
// Cambiar a estatus de inicio
ipcMain.on('initstate',(event,args)=>{
    //
    event.returnValue=initstate
});


// Obtener lista de carreras
ipcMain.on('obtener-carreras-asincrono',(event,args)=>{
    let scc=new sccUtil(dbPath);
    event.reply('r-obtener-carreras-asincrono',scc.obtenerCarreras())
});

// Obtener periodos asincronos
ipcMain.on('obtener-periodos-asincrono',(event,args)=>{
    let scc=new sccUtil(dbPath);
    event.reply('r-obtener-periodos-asincrono',scc.obtenerPeriodos())
});
// Obtener Lista de alumnos sincrono
ipcMain.on('obtener-alumnos-sincrono',(event,args)=>{
    let calu=new alumnoController(dbPath);
    let res=calu.obtenerAlumnos();

    event.returnValue=res
});
// Obtener lista de alumnos asincrona
ipcMain.on('obtener-alumnos-asincrono',(event,args)=>{
    let calu=new alumnoController(dbPath);
    let res=calu.obtenerAlumnos(args);
    //;
    event.reply('r-obtener-alumnos-asincrono',res);
});

ipcMain.on('obtener-alumno-asincrono',(event,args)=>{
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let alu=calu.obtenerAlumno(args);
    let act=cact.obtenerActividades(alu.alu_ncontrol);
    let res={alumno:alu,actividades:act};
    //;
    event.reply('r-obtener-alumno-asincrono',res);
});


// Eliminar a un alumno  de manera asincrona
ipcMain.on('eliminar-alumno-asincrono',(event,args)=>{
    let calu=new alumnoController(dbPath)
    event.reply('r-eliminar-alumno-asincrono',calu.eliminarAlumno(args));
});

// evento de preeliminacion
ipcMain.on('preEliminar-alumno',(event,args)=>{
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let ccar=new carreraController(dbPath);
    let alumno=calu.obtenerAlumno(args);
    let actividades=cact.obtenerActividades(args);
    let carrera=ccar.obtenerCarrera(alumno.car_id);
    event.reply('r-preEliminar-alumno',{
        nombre:alumno.alu_nombre,
        ncontrol:args,
        carrera:carrera.car_nombre,
        actividades:actividades
    });
});


// crear alumno de manera asincrona
ipcMain.on('crear-alumno-asincrono',(event,{alumno,actividades})=>{
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let res=calu.crearAlumno(alumno);
    if(res.estatus)
        cact.crearActividades(alumno.ncontrol,actividades);
    event.reply('r-crear-alumno-asincrono',res);
});

// editar alumno asincrono
ipcMain.on('editar-alumno-asincrono',(event,{alumno,actividades})=>{
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let res=calu.editarAlumno(alumno);
    if(res.estatus)
    {
        cact.eliminarActividades(alumno.ncontrol)
        cact.crearActividades(alumno.ncontrol,actividades);
    }
    event.reply('r-editar-alumno-asincrono',res);
});
ipcMain.on('buscar-alumnos-asincrono',(event,args)=>{
    let calu=new alumnoController(dbPath);
    let output=[]
    if(args==" "||args=="")
        output=calu.obtenerAlumnos(200);
    else
        output=calu.filtrarAlumnos(args)
    event.reply('r-buscar-alumnos-asincrono',output);
})
ipcMain.on('leer-alumnos-excel-asincrono',(event,args)=>{
    const readXlsxFile = require('read-excel-file/node');
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    // File path.
    readXlsxFile(args,{sheet:'alumnos'}).then((rows) => {
        let i=0;
        event.reply('r-progreso-set-lim',rows.length);
        event.reply('r-progreso-set-titulo','Registrando Alumnos...');
        rows.forEach(row => {
            if(i)
                calu.crearAlumno({ncontrol:row[0],nombre:`${row[1]} ${row[2]} ${row[3]}`,carrera:row[4]});
            i++
            event.reply('r-progreso',i);
        });
    })




    readXlsxFile(args,{sheet:'actividades'}).then((rows) => {
        let i=0;
        event.reply('r-progreso-set-lim',rows.length);
        event.reply('r-progreso-set-titulo','Registrando Actividades...');
        rows.forEach(row => {
            if(i)
            {
                //
                let periodo=row[2].split(' ')[0].match(/ago-dic/gmi)!=null?2:1;
                let anio=row[2].split(' ')[1]
                cact.crearActividad({ncontrol:row[0],nombre:row[1],periodo:periodo,anio:anio,creditos:row[3]});
            }
            i++
            event.reply('r-progreso',i);
        });
    })

    event.reply('r-leer-alumnos-excel-asincrono',null);
});

ipcMain.on('obtener-paginacion-asincrona',(event,args)=>{
    let calu=new alumnoController(dbPath);
    event.reply('r-obtener-paginacion-asincrona',calu.generarPaginacion())
});

ipcMain.on('preConstancia-alumno',(event,args)=>{
    // objetos a utilizar
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let alu=calu.obtenerAlumno(args);
    event.reply('r-preConstancia-alumno',{
        ncontrol:args,
        nombre:alu.alu_nombre,
        carrera:alu.car_id,
        actividades:cact.obtenerActividades(args)
    });
});

ipcMain.on('crear-constancia-asincrono',(event,args)=>{
    let ccst=new constanciaController(dbPath);
    let ccon=new configuracionController(dbPath);
    event.reply('r-crear-constancia-asincrono',ccst.crearConstancia(args));
    ccon.actualizarFolio(ccon.obtenerFolio().con_folio+1);
});


ipcMain.on('obtener-constancias-asincrono',(event,args)=>{
    let ccst=new constanciaController(dbPath);
    event.reply('r-obtener-constancias-asincrono',ccst.obtenerConstancias());
})

ipcMain.on('guardar-configuracion-asincrono',(event,args)=>{
    let ccon=new configuracionController(dbPath);
    event.reply('r-guardar-configuracion-asincrono',ccon.actualizarConfiguracion(args));
})
ipcMain.on('preview-configuracion-asincrono',(event,args)=>{
    let ccst=new constanciaController(dbPath);
    ccst.genPDFTest(app,args,(url)=>{
        let pdf_window=new pdf_viewer({
            width:800,
            height:600,
            show:false
        })
        pdf_window.loadURL(url);
        pdf_window.once('ready-to-show',()=>{
            pdf_window.show();
        })
    })
    event.reply('nada',null);
})

ipcMain.on('imprimir-constancia-asincrono',(event,args)=>{
    
    let ccst=new constanciaController(dbPath);
    ccst.genPDF(app,args,(url)=>{
        let pdf_window=new pdf_viewer({
            width:800,
            height:600,
            show:false
        })
        pdf_window.loadURL(url);
        pdf_window.once('ready-to-show',()=>{
            pdf_window.show();
        })
    })
    event.reply('nada',null);
})

function startApp()
{
    debug = false;
    // Revisar si es la primera vez que se inicia la app
    openWindow("./vistas/splash",500,300,500,300,true,false);
    let appconfg=require('./initConfig.json')
    //if(debug)appController.destroyInit(app,appconfg);
    if(!appController.fileVerification(app,appconfg))
    {
        appController.initApp(app,appconfg);
        // inicializando base de datos
        let fs=require('fs');
        let sqlite=require('better-sqlite3');
        let db=sqlite(dbPath);
        let defschema=fs.readFileSync(`${app.getPath('userData')}/Configuracion/schema.sql`,'utf8');
        db.exec(defschema);
        db.close();
        initstate=true
    }
    else
        initstate=true
}
function tests()
{
    let ccst=new constanciaController(dbPath);
    let calu=new alumnoController(dbPath);
    let cact=new actividadController(dbPath);
    let ccon= new configuracionController(dbPath);
    ccst.genPDFTest(app,null,(url)=>{
        let pdf_window=new pdf_viewer({
            width:800,
            height:600,
            show:false
        })
        pdf_window.loadURL(url);
        pdf_window.once('ready-to-show',()=>{
            pdf_window.show();
        })
    })
    
    ;
    ;
    //Generar alumno
    calu.crearAlumno({
        ncontrol:"15130686",
        nombre:"DAMIAN CARDENAS QUIÑONES",
        carrera:1
    });
    calu.crearAlumno({
        ncontrol:"15130687",
        nombre:"Vale Verguin",
        carrera:1
    });
    // Generar actividades
    cact.eliminarActividades("15130686");
    cact.crearActividades("15130686",[
        {
            nombre:"Taller de Linux",
            periodo:2,
            anio:2016,
            creditos:0.5
        },
        {
            nombre:"Taller de Arduino",
            periodo:1,
            anio:2017,
            creditos:2
        },
        {
            nombre:"Asistencia Foro 2018",
            periodo:2,
            anio:2018,
            creditos:1
        },
        {
            nombre:"Participación Mining_Hack 2018",
            periodo:2,
            anio:2018,
            creditos:1
        },
        {
            nombre:"Participación Eneit Innovación Social Etapa Local",
            periodo:1,
            anio:2019,
            creditos:1
        }
    ]);
    cact.crearActividades("15130687",[
        {
            nombre:"Taller de valer verga",
            periodo:2,
            anio:2016,
            creditos:2
        }
    ]);
    calu.eliminarAlumno("15130687");
    
}


app.on("ready", () => {
    startApp()
    //if(debug)tests();
});

app.on("window-all-closed", () => {
    app.quit()
});








module.exports = { openWindow };
