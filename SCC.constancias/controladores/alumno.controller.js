const sqlite=require('better-sqlite3');
const actividadController=require('./actividad.controller');

class alumnoController
{
	path="";
	paginacion=200;
	constructor(path)
	{
		this.path=path
	}
	crearAlumno({ncontrol="",nombre="",carrera=0})
 	{
		let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from alumnos where alu_ncontrol=${ncontrol}`)
		if(stmt.get().cantidad_registros<1)
		{
			stmt=db.prepare(`insert into alumnos (alu_ncontrol,alu_nombre,car_id) values("${ncontrol}","${nombre}",${carrera})`);
			stmt.run();
			db.close();
			return {estatus:1,mensaje:"Transaccion de creacion exitosa!"};
		}
		db.close()
		return {estatus:0,mensaje:"Alumno existente..."};
	 }

	 editarAlumno({ncontrol="",nombre="",carrera=0})
	 {
		let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from alumnos where alu_ncontrol=${ncontrol}`)
		if(stmt.get().cantidad_registros==1)
		{
			stmt=db.prepare(`update alumnos set alu_nombre="${nombre}", car_id=${carrera} where alu_ncontrol="${ncontrol}"`);
			stmt.run();
			db.close()
			return {estatus:1,mensaje:"Transaccion de edicion exitosa!"};
		}
		db.close()
		return {estatus:0,mensaje:"Alumno inexistente..."};
	 }
	 eliminarAlumno(ncontrol=""){
		let cact=new actividadController(this.path);
		cact.eliminarActividades(ncontrol);
		let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from alumnos where alu_ncontrol=${ncontrol}`)
		if(stmt.get().cantidad_registros==1)
		{
			stmt=db.prepare(`delete from alumnos  where alu_ncontrol="${ncontrol}";`);
			stmt.run();
			db.close()
			return {estatus:1,mensaje:"Transaccion de eliminacion exitosa!"};
		}
		db.close()
		return {estatus:0,mensaje:"Alumno inexistente..."};
	 }
	 filtrarAlumnos(data=""){
		let db=new sqlite(this.path);
		let q=`select *  from alumnos where alu_ncontrol like "%${data}%" or alu_nombre like "%${data}%" `
		let stmt=db.prepare(q)
		let result=stmt.all()
		db.close()
		return result;
	 }
	 obtenerAlumno(ncontrol="")
	 {
		let db=new sqlite(this.path);
		let stmt=db.prepare(`select *  from alumnos where alu_ncontrol="${ncontrol}" `)
		let result=stmt.all()[0]
		db.close()
		return result;
	 }
	 obtenerAlumnos(pag)
	 {
		let db=new sqlite(this.path);
		let stmt=db.prepare(`select * from alumnos limit ${this.paginacion} offset ${this.paginacion*pag}`);
		let result=stmt.all()
		db.close()
		return result;
	 }
	 generarPaginacion(limite=this.paginacion)
	 {
		 this.paginacion=limite;
		 let db=new sqlite(this.path);
		 let stmt=db.prepare(`select count(*) as n from alumnos`);
		 let result=stmt.get()
		 db.close()
		 return Math.round(result.n/this.paginacion);
	 }

}
module.exports=alumnoController;
