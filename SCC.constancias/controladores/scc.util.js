const sqlite=require('better-sqlite3');
class sccUtil{
    constructor(path)
    {
        this.path=path
    }
    obtenerCarreras()
    {
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select *  from carreras`);
		return stmt.all();
    }
    obtenerCarrera(id=0)
    {
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select * from carreras where car_id=${id}`);
		return stmt.get();
    }
    obtenerPeriodos()
    {
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select *  from periodos`);
		return stmt.all();
    }
}
module.exports=sccUtil