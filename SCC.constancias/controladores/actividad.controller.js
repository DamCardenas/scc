const sqlite=require('better-sqlite3');
class actividadController{
    constructor(path)
    {
        this.path=path
    }
    crearActividad({ncontrol="",nombre="",periodo=0,anio=0,creditos=0.0})
    {
        let db=new sqlite(this.path);
        let q=`insert into actividades (alu_ncontrol,act_nombre,per_id,act_anio,act_creditos) values("${ncontrol}","${nombre}",${periodo},${anio},${creditos})`
        //;
        let stmt=db.prepare(q);
        stmt.run();
        db.close();
        return {estatus:1,mensaje:"Transaccion de Creacion exitosa!"};
    }
    crearActividades(ncontrol="",actividades=[])
    {
        let db=new sqlite(this.path);
        for (let i = 0; i < actividades.length; i++) {
            const element = actividades[i];
            let stmt=db.prepare(`insert into actividades (alu_ncontrol,act_nombre,per_id,act_anio,act_creditos) values("${ncontrol}","${element.nombre}",${element.periodo},${element.anio},${element.creditos})`);
            stmt.run();      
            
        }
        db.close();
        return {estatus:1,mensaje:"Transaccion de Creacion exitosa!"};
    }
    editarActividad({id=0,nombre="",periodo=0,anio=0,creditos=0.0}){
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from actividades where act_id=${id}`)
		if(stmt.get().cantidad_registros==1)
		{
			stmt=db.prepare(`update actividades set act_nombre="${nombre}", per_id=${periodo}, act_anio=${anio}, act_creditos=${creditos} where act_id=${id}`);
            stmt.run();
            db.close()
			return {estatus:1,mensaje:"Transaccion de Edicion exitosa!"};
		}
        db.close()
		return {estatus:0,mensaje:"Actividad inexistente..."};
    }
    eliminarActividad(id=0){
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from actividades where act_id=${id}`)
		if(stmt.get().cantidad_registros==1)
		{
            stmt=db.prepare(`delete from actividades where act_id=${id}`);
			stmt.run();
            db.close()
			return {estatus:1,mensaje:"Transaccion de Eliminacion exitosa!"};
		}
        db.close()
		return {estatus:0,mensaje:"Actividad inexistente..."};
    }
    eliminarActividades(ncontrol="")
    {
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select count(*) as cantidad_registros from actividades where alu_ncontrol=${ncontrol}`)
		if(stmt.get().cantidad_registros>=1)
		{
            stmt=db.prepare(`delete from actividades where alu_ncontrol=${ncontrol}`);
			stmt.run();
            db.close()
			return {estatus:1,mensaje:"Transaccion de Eliminacion exitosa!"};
		}
        db.close()
		return {estatus:0,mensaje:"Actividad inexistente..."};
    }
    obtenerActividades_mod(act_id=[])
    {
        let query="select * from actividades where act_id="+act_id[0].act_id;
        for (let o = 1; o < act_id.length; o++)
            query+=` or act_id=${act_id[o].act_id}`;
        //
        let db=new sqlite(this.path);
        let res=db.prepare(query).all();
        db.close();
        return res;
    }
    obtenerActividades(ncontrol="")
    {
        let db=new sqlite(this.path);
		let stmt=db.prepare(`select *  from actividades where alu_ncontrol="${ncontrol}"`);
		let result=stmt.all()
		db.close()
		return result;
    }
}
module.exports=actividadController;