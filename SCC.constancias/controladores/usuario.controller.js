const sqlite=require('better-sqlite3');
class usuarioController
{
    constructor(path)
    {
        this.path=path;
    }
    verificarUsuario({usu_email,usu_password}){}
    crearUsuario({usu_email,usu_nombre,usu_password,tus_id}){}
    editarUsuario({usu_id,usu_email,usu_nombre,usu_password,tus_id}){}
    eliminarUsuario(usu_id){}
    obtenerUsuario(usu_id){}
    obtenerUsuarios(){}
    obtenerPrivilegios(usu_id){}
    obtenerCarreras(usu_id){}
}
module.exports=usuarioController;