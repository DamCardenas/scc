const sqlite=require('better-sqlite3');
class carreraController
{
    constructor(path)
    {
        this.path=path;
    }
    crearCarrera({car_nombre,car_departamento,car_siglas,car_jefe_departamento,car_frase_departamento})
    {
        let db=new sqlite(this.path);
        let stmt =db.prepare(`insert into carreras(car_nombre,car_departamento,car_siglas,car_jefe_departamento,car_frase_departamento) values("${car_nombre}","${car_departamento}","${car_siglas}","${car_jefe_departamento}",,"${car_frase_departamento}")`)
        let res=stmt.run();
        db.close()
        return res;
    }
    editarCarrera({car_id,car_nombre,car_departamento,car_siglas,car_jefe_departamento,car_frase_departamento}){
        let db=new sqlite(this.path);
        let stmt =db.prepare(`update carreras set car_nombre="${car_nombre}",car_departamento="${car_departamento}",car_siglas="${car_siglas}",car_jefe_departamento="${car_jefe_departamento}",car_frase_departamento="${car_frase_departamento}" where car_id=${car_id}`);
        let res=stmt.run();
        db.close()
        return res;
    }
    eliminarCarrera(car_id){
        let db=new sqlite(this.path);
        let stmt =db.prepare(`delete from carreras  where car_id=${car_id}`);
        let res=stmt.run();
        db.close()
        return res;
    }
    obtenerCarrera(car_id){
        let db=new sqlite(this.path);
        let stmt =db.prepare(`select * from carreras where car_id="${car_id}"`);
        let res=stmt.all()[0];
        db.close()
        return res;
    }
    obtenerCarreras(){
        let db=new sqlite(this.path);
        let stmt =db.prepare(`select * from carreras`);
        let res=stmt.all();
        db.close()
        return res;
    }
}
module.exports=carreraController;