const sqlite=require('better-sqlite3');

// Controlador para configuracion de la db
class configuracionController{
    path="";
	constructor(path)
	{
		this.path=path
    }
    obtenerConfiguracion()
    {
        let db=new sqlite(this.path);
        let res=db.prepare(`select * from configuracion where con_id=1`).all()[0];
        db.close();
        return res;
    }
    obtenerFolio(){
        let db=new sqlite(this.path);
        let res=db.prepare(`select con_folio from configuracion where con_id=1`).all()[0];
        db.close();
        return res;
    }
    obtenerFondo(){
        let db=new sqlite(this.path);
        let res=db.prepare(`select con_fondo from configuracion where con_id=1`).all()[0].con_fondo;
        db.close();
        return res;
    }
    obtenerCabecera(){
        let db=new sqlite(this.path);
        let res=db.prepare(`select con_cabecera from configuracion where con_id=1`).all()[0].con_cabecera;
        db.close();
        return res;
    }
    obtenerPie(){
        let db=new sqlite(this.path);
        let res=db.prepare(`select con_pie from configuracion where con_id=1`).all()[0].con_pie;
        db.close();
        return res;
    }
    actualizarFolio(folio)
    {
        let db=new sqlite(this.path);
        let res=db.prepare(`update configuracion set con_folio=${folio} where con_id=1`).run();
        db.close();
        return res;
    }
    actualizarConfiguracion({con_folio,con_frase_anual,con_jefe_servicios_escolares,con_contenido,con_cabecera,con_pie,con_fondo})
    {
        let db=new sqlite(this.path);
        let res=db.prepare(`update configuracion set con_folio=${con_folio}, con_frase_anual="${con_frase_anual}",con_jefe_servicios_escolares="${con_jefe_servicios_escolares}",con_contenido="${con_contenido}",con_cabecera="${con_cabecera}",con_pie="${con_pie}",con_fondo="${con_fondo}" where con_id=1`).run();
        db.close();
        return res;
    }
}
module.exports=configuracionController;