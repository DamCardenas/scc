const sqlite=require('better-sqlite3');
let pdfkit=require('pdfkit');

const alumnoController=require('./alumno.controller');
const configuracionController=require('./configuracion.controller');
const carreraController=require('./carrera.controller');
const actividadController=require('./actividad.controller')
const base_config={
    fondo:{
        posicion: {
            x: 0,
            y: 0
        },
        dimencion: {
            ancho: 612,
            largo: 792
        }
    },
    cabezera: {
        posicion: {
            x: 20,
            y: 15
        },
        dimencion: {
            ancho: 572,
            largo: 103
        }
    },
    pie: {
        posicion: {
            x: 20,
            y: 686
        },
        dimencion: {
            ancho: 572,
            largo: 84
        }
    }
}


function givemepos(str){
  let temp=str
  let flag=false
  let output=[]
  while(!flag)
  {
      let a=temp.search('{')
      let b=temp.search('}')
      if(a>=0&&b>=0)
      {
          temp=temp.replace('{',' ')
          temp=temp.replace('}',' ')
          output.push([a,b])
      }
      else
          flag=true
  }
  return output
}
function givemeids(ranges=[],text="")
{
  let output=[]
  for (let index = 0; index < ranges.length; index++) {
      const element = ranges[index];
      output.push(text.slice(element[0]+1,element[1]))
  }
  return output;
}

// Modificar funcion
function givemevalues(str,alumno,carrera,configuracion,constancia)
{
  let ids=givemeids(givemepos(str),str);
  let output=[]
  for (let index = 0; index < ids.length; index++) {
      const element = ids[index];
      switch (element) {
          case 'jde': 
              output.push(carrera.car_frase_departamento)
              break;
          case 'alu-ncontrol': 
              output.push(alumno.alu_ncontrol)
              break;
          case 'alu-nombre': 
              output.push(alumno.alu_nombre)
              break;
          case 'alu-carrera': 
              output.push(carrera.car_nombre)
              break;
      }
  }
  return output;
}

function agregarCeros(str="")
{
    let ceros=8-str.length;
    let output=str;
    for(let i=0;i<ceros;i++)
        output="0"+output;
    return output;
}
class  constanciaController
{
    constructor(path)
    {
        this.path=path;
    }
    generarFolio(car_id)
    {
        let ccar=new carreraController(this.path);
        let ccon=new configuracionController(this.path);
        let fecha=new Date();
        let carrera=ccar.obtenerCarrera(car_id);
        let folio=ccon.obtenerFolio().con_folio;
        return `${fecha.getFullYear().toString().slice(2,4)}${carrera.car_siglas}C${agregarCeros(""+folio)}`;
    }
    crearConstancia({alu_ncontrol,actividades,car_id})
    {   
        let db=new sqlite(this.path);
        let fecha=new Date();
        let meses=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre']
        let fecha_str=`${fecha.getDate()} de ${meses[fecha.getMonth()]} del ${fecha.getFullYear()}`
        let res=db.prepare(`insert into constancias (cst_id,alu_ncontrol,cst_fecha,cst_actividad) values('${this.generarFolio(car_id)}','${alu_ncontrol}','${fecha_str}','${JSON.stringify(actividades)}')`).run();
        db.close();
        return res;   
    }

    obtenerConstancia(ncontrol="")
    {
        let db=new sqlite(this.path);
        let res=db.prepare(`select * from constancias where alu_ncontrol="${ncontrol}"`).all()[0];
        db.close();
        return res;
    }
    obtenerConstancias()
    {
        let db=new sqlite(this.path);
        let res=db.prepare(`select * from constancias`).all();
        db.close();
        return res;
    }

    acredorConstancia(ncontrol="")
    {
        let db=new sqlite(this.path);
        let stmt=db.prepare(`select sum(act_creditos) as total_creditos from actividades where alu_ncontrol="${ncontrol}"`);
        let result=stmt.get();
        db.close();
        return result.total_creditos>=5.0;
    }

    genPDFTest(app,configuracion,callback)
    {
        let ccon=new configuracionController(this.path);
        // Ensamble de alumno
        let alumno={
            alu_ncontrol:"00000000",
            alu_nombre:"NOMBRE DEL ALUMNO"
        }

        // Ensamble de carrera
        let carrera={
            car_nombre:"NOMBRE DE CARRERA",
            car_departamento:"DEPARTAMENTO",
            car_frase_departamento:"FRASE DE DEPARTAMENTO",
            car_jefe_departamento:"NOMBRE DEL JEFE DE DEPARTAMENTO"
        }
        // Ensamble de actividades
        let actividades=[
            {
                act_nombre:"ACTIVIDAD 1",
                per_id:2,
                act_anio:2016,
                act_creditos:2
            },
            {
                act_nombre:"ACTIVIDAD 2",
                per_id:1,
                act_anio:2017,
                act_creditos:2
            },
            {
                act_nombre:"ACTIVIDAD 3",
                per_id:2,
                act_anio:2018,
                act_creditos:1
            }
        ]
        // Ensamble constancia
        let constancia={
            cst_id: '19S. y C.C00000001',
            alu_ncontrol: '15130686',
            cst_fecha: '10 de Diciembre del 2019',
            cst_actividad: JSON.parse('[{"act_creditos":"2","act_id":"2"},{"act_creditos":"1","act_id":"3"},{"act_creditos":"1","act_id":"4"},{"act_creditos":"1","act_id":"5"}]')
        };
        // Ensamble Configuracion
        let config={};
        if(configuracion!=null)
            config=configuracion;
        else
            config={
                con_frase_anual:"FRASE ANUAL",
                con_jefe_servicios_escolares:"NOMBRE DEL JEFE DE SERVICIOS ESCOLARES",
                con_contenido:"El que suscribe {jde}, por este medio se permite hacer de su conocimiento que el estudiante {alu-nombre} con Número de Control {alu-ncontrol} de la carrera de {alu-carrera} ha ACREDITADO las siguientes actividades complementarias durante los periodos escolares que se enumeran, con un valor curricular de 5 créditos.",
                con_cabecera:ccon.obtenerCabecera(),
                con_pie:ccon.obtenerPie(),
                con_fondo:ccon.obtenerFondo()
            };
        // Generar PDF
        let doc=new pdfkit();
        let fs=require('fs');
        let stream=doc.pipe(fs.createWriteStream(`${app.getPath('userData')}/preview.pdf`));
        //insertar imagenes
        // Fondo
        doc.image(new Buffer(config.con_fondo.replace('data:image/png;base64,',''), 'base64'),base_config.fondo.posicion.x,base_config.fondo.posicion.y,{fit:[base_config.fondo.dimencion.ancho,base_config.fondo.dimencion.largo]})
        // Cabecera
        doc.image(new Buffer(config.con_cabecera.replace('data:image/png;base64,',''), 'base64'),base_config.cabezera.posicion.x,base_config.cabezera.posicion.y)
        // Pie de pagina
        doc.image(new Buffer(config.con_pie.replace('data:image/png;base64,',''), 'base64'),base_config.pie.posicion.x,base_config.pie.posicion.y)

        doc.font('Courier')
        doc.fontSize(9);
        
        // Generacion frase anual
        doc.text(`"${config.con_frase_anual}"`,20,123,{width:572,align:'center'});
        // Nombre del documento

        doc.font('Courier-Bold').text(`Constancia de Acreditacion de Actividades Complementarias`,20,153,{width:572,align:'center'}).font('Courier');
        // Lugar, fecha y folio

        doc.fontSize(10);
        doc.text(`Torreón, Coahuila\n${constancia.cst_fecha}\nOficio NO. ${constancia.cst_id}`,20,193,{width:552,align:'right'});
        // Jefe de servicios escolares
        doc.font('Courier-Bold').text(`${config.con_jefe_servicios_escolares}\nJefe de Departamento de Servicios Escolares\nPresente`,40,193,{width:286,align:'left'}).font('Courier');
        
        
        // Texto a Generar
        doc.fontSize(11);
        doc.text('',40,263,{width:532,align:'justify',continued:true})
        let text=config.con_contenido;
        let array=text.split(/\{[a-zA-Z\-]*\}/g);
        let val=givemevalues(text,alumno,carrera);
        let cts=[0,0];
        while(cts[0]<array.length||cts[1]<val.length)
        {
            if(cts[0]<array.length)
            {
                doc.font('Courier').text(array[cts[0]],{continued:true})
                cts[0]+=1;
            }
            if(cts[1]<val.length)
            {
                doc.font('Courier-Bold').text(val[cts[1]],{continued:true})
                doc.font('Courier').text(' ',{continued:true});
                cts[1]+=1;
            }                
        }
        // Listado de actividades Complementarias
        doc.text('',50,300,{align:'left',underline:true,continued:false});
        doc.moveDown();
        doc.font('Courier')
        doc.text('Actividad',40,340,{align:'left',underline:true});
        doc.text('Periodo',380,340,{align:'left',underline:true});
        doc.text('Creditos',480,340,{align:'left',underline:true});
        let delta=12;
        for (let index = 0; index < actividades.length; index++) {
            const element = actividades[index];
            doc.text(element.act_nombre,40,340+delta*(index+1),{align:'left'});
            doc.text(`${(element.per_id==1?'Ene-Jun':'Ago-Dic')} ${element.act_anio}`,380,340+delta*(index+1),{align:'left'});
            doc.text(`${element.act_creditos}`,480,340+delta*(index+1),{align:'left'});
        }
        doc.font('Courier-Bold')
        doc.text('ATENTAMENTE',45,475,{align:'left'});
        doc.font('Courier')
        doc.fontSize(9);
        // Frase de departamento
        doc.text(`"${carrera.car_frase_departamento}"`,45,485,{align:'left'});
        doc.fontSize(12);
        doc.font('Courier-Bold')
        doc.text(`${carrera.car_jefe_departamento}\nJefe de Departamento de ${carrera.car_departamento}`,40,600,{width:286,align:'left'})
        doc.end();
        stream.on('close',()=>{
            callback(`${app.getPath('userData')}/preview.pdf`);
        })
    }
    
    genPDF(app,ncontrol,callback)
    {
        let ccon=new configuracionController(this.path);
        let calu=new alumnoController(this.path);
        let cact=new actividadController(this.path);
        let ccar=new carreraController(this.path);
        
        let config=ccon.obtenerConfiguracion();
        
        let alumno=calu.obtenerAlumno(ncontrol);
        let carrera=ccar.obtenerCarrera(alumno.car_id);
        let constancia=this.obtenerConstancia(ncontrol);
        let actemp=JSON.parse(constancia.cst_actividad);
        let actividades=cact.obtenerActividades_mod(actemp);
        for (let i = 0; i < actividades.length; i++)
            actividades[i].act_creditos=actemp[i].act_creditos;
        // Generar PDF
        let doc=new pdfkit();
        let fs=require('fs');
        let stream=doc.pipe(fs.createWriteStream(`${app.getPath('userData')}/preview.pdf`));
        //insertar imagenes
        // Fondo
        doc.image(new Buffer(config.con_fondo.replace('data:image/png;base64,',''), 'base64'),base_config.fondo.posicion.x,base_config.fondo.posicion.y,{fit:[base_config.fondo.dimencion.ancho,base_config.fondo.dimencion.largo]})
        // Cabecera
        doc.image(new Buffer(config.con_cabecera.replace('data:image/png;base64,',''), 'base64'),base_config.cabezera.posicion.x,base_config.cabezera.posicion.y)
        // Pie de pagina
        doc.image(new Buffer(config.con_pie.replace('data:image/png;base64,',''), 'base64'),base_config.pie.posicion.x,base_config.pie.posicion.y)

        doc.font('Courier')
        doc.fontSize(9);
        
        // Generacion frase anual
        doc.text(`"${config.con_frase_anual}"`,20,123,{width:572,align:'center'});
        // Nombre del documento

        doc.font('Courier-Bold').text(`Constancia de Acreditacion de Actividades Complementarias`,20,153,{width:572,align:'center'}).font('Courier');
        // Lugar, fecha y folio

        doc.fontSize(10);
        doc.text(`Torreón, Coahuila\n${constancia.cst_fecha}\nOficio NO. ${constancia.cst_id}`,20,193,{width:552,align:'right'});
        // Jefe de servicios escolares
        doc.font('Courier-Bold').text(`${config.con_jefe_servicios_escolares}\nJefe de Departamento de Servicios Escolares\nPresente`,40,193,{width:286,align:'left'}).font('Courier');
        
        
        // Texto a Generar
        doc.fontSize(11);
        doc.text('',40,263,{width:532,align:'justify',continued:true})
        let text=config.con_contenido;
        let array=text.split(/\{[a-zA-Z\-]*\}/g);
        let val=givemevalues(text,alumno,carrera);
        let cts=[0,0];
        while(cts[0]<array.length||cts[1]<val.length)
        {
            if(cts[0]<array.length)
            {
                doc.font('Courier').text(array[cts[0]],{continued:true})
                cts[0]+=1;
            }
            if(cts[1]<val.length)
            {
                doc.font('Courier-Bold').text(val[cts[1]],{continued:true})
                doc.font('Courier').text(' ',{continued:true});
                cts[1]+=1;
            }                
        }
        // Listado de actividades Complementarias
        doc.text('',50,300,{align:'left',underline:true,continued:false});
        doc.moveDown();
        doc.font('Courier')
        doc.text('Actividad',40,340,{align:'left',underline:true});
        doc.text('Periodo',380,340,{align:'left',underline:true});
        doc.text('Creditos',480,340,{align:'left',underline:true});
        let delta=12;
        for (let index = 0; index < actividades.length; index++) {
            const element = actividades[index];
            doc.text(element.act_nombre,40,340+delta*(index+1),{align:'left'});
            doc.text(`${(element.per_id==1?'Ene-Jun':'Ago-Dic')} ${element.act_anio}`,380,340+delta*(index+1),{align:'left'});
            doc.text(`${element.act_creditos}`,480,340+delta*(index+1),{align:'left'});
        }
        doc.font('Courier-Bold')
        doc.text('ATENTAMENTE',45,475,{align:'left'});
        doc.font('Courier')
        doc.fontSize(9);
        // Frase de departamento
        doc.text(`"${carrera.car_frase_departamento}"`,45,485,{align:'left'});
        doc.fontSize(12);
        doc.font('Courier-Bold')
        doc.text(`${carrera.car_jefe_departamento}\nJefe de Departamento de ${carrera.car_departamento}`,40,600,{width:286,align:'left'})
        doc.end();
        stream.on('close',()=>{
            callback(`${app.getPath('userData')}/preview.pdf`);
        })
    }

     
}
module.exports=constanciaController;
