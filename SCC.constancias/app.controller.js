class appController
{
    static fileVerification(app,{dirs=[],files=[]})
    {
        let fs=require('fs');
        for (let index = 0; index < dirs.length; index++) {
            const element = dirs[index];
            if (!fs.existsSync(`${app.getPath("userData")}/${element}`))return false
        }
        for (let j = 0; j < files.length; j++) {
            const element = files[j];
            for (let i = 0; i < element.path.length; i++) {
                const path = element.path[i];
                if (!fs.existsSync(`${app.getPath("userData")}/${path}`))return false
            }
        }    
        return true;
    }
    static destroyInit(app,{dirs=[],files=[]})
    {
        let fs=require('fs');
        let rm=require('rimraf');
        //Eliminacion de arbol de directorios
        for (let index = 0; index < dirs.length; index++) {
            const element = dirs[index];
            if(fs.existsSync(`${app.getPath("userData")}/${element}`))
                rm.sync(`${app.getPath("userData")}/${element}`);
        }
    }

    static initApp(app,{dirs=[],files=[]})
    {
        let fs=require('fs');
        //Creacion de arbol de directorios
        for (let index = 0; index < dirs.length; index++) {
            const element = dirs[index];
            if(!fs.existsSync(`${app.getPath("userData")}/${element}`))
                fs.mkdirSync(`${app.getPath("userData")}/${element}`);
        }
        //Creacion de archivos
        for (let j = 0; j < files.length; j++) {
            const element = files[j];
            for (let i = 0; i < element.path.length; i++) {
                const path= element.path[i];
                if(!fs.existsSync(`${app.getPath("userData")}/${path}`))
                    fs.writeFileSync(`${app.getPath("userData")}/${path}`,element.content)
            }
        }        
    }

}
module.exports=appController;