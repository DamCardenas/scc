## Classes

<dl>
<dt><a href="#configController">configController</a></dt>
<dd></dd>
<dt><a href="#constanciasController">constanciasController</a></dt>
<dd></dd>
</dl>

<a name="configController"></a>

## configController
**Kind**: global class  

* [configController](#configController)
    * [.loadFile(configFilePath)](#configController.loadFile) ⇒ <code>Object</code>
    * [.getFolio(configFilePath)](#configController.getFolio) ⇒ <code>int</code>
    * [.setFolio(configFilePath, folio, configFilePath, folio)](#configController.setFolio) ⇒ <code>void</code>
    * [.getCabecera(configFilePath, configFilePath)](#configController.getCabecera) ⇒ <code>Object</code> \| <code>Object</code>
    * [.getPie(configFilePath)](#configController.getPie) ⇒ <code>Object</code>
    * [.getCampos(configFilePath)](#configController.getCampos) ⇒ <code>Object</code>
    * [.setCampos(configFilePath, campos)](#configController.setCampos)
    * [.saveFile(configObject, configFilePath)](#configController.saveFile)
    * [.restoreConfigFile(configFilePath)](#configController.restoreConfigFile)
    * [.restoreDefaultConfigFile(configFilePath)](#configController.restoreDefaultConfigFile)

<a name="configController.loadFile"></a>

### configController.loadFile(configFilePath) ⇒ <code>Object</code>
Lee un archivo JSON y retorna un objeto JS de dicho archivo

**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 

<a name="configController.getFolio"></a>

### configController.getFolio(configFilePath) ⇒ <code>int</code>
Obtiene el folio actual del archivo de configuracion (configFilePath)
(Test: OK)

**Kind**: static method of [<code>configController</code>](#configController)  
**Returns**: <code>int</code> - que indica el folio actual  

| Param | Type |
| --- | --- |
| configFilePath | <code>\*</code> | 

<a name="configController.setFolio"></a>

### configController.setFolio(configFilePath, folio, configFilePath, folio) ⇒ <code>void</code>
Realiza un cambio de folio al archivo de configuracion (configFilePath)
(Test: OK)
    /**

**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 
| folio | <code>Int</code> | 
| configFilePath | <code>String</code> | 
| folio | <code>Int</code> | 

<a name="configController.getCabecera"></a>

### configController.getCabecera(configFilePath, configFilePath) ⇒ <code>Object</code> \| <code>Object</code>
Retorna un objeto con la configuracion de la cabezera del archivo de configuracion proporcionado
    /**

**Kind**: static method of [<code>configController</code>](#configController)  
**Returns**: <code>Object</code> - con la configuracion de la cabecera<code>Object</code>  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 
| configFilePath | <code>String</code> | 

<a name="configController.getPie"></a>

### configController.getPie(configFilePath) ⇒ <code>Object</code>
**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 

<a name="configController.getCampos"></a>

### configController.getCampos(configFilePath) ⇒ <code>Object</code>
**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 

<a name="configController.setCampos"></a>

### configController.setCampos(configFilePath, campos)
**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>String</code> | 
| campos | <code>Array</code> | 

<a name="configController.saveFile"></a>

### configController.saveFile(configObject, configFilePath)
Recibe un objeto con la configuracion que se desea guardar y el path del archivo donde se desea guardar.
genera una copia del archivo de configuracion anterior
(Test: OK)

**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configObject | <code>Object</code> | 
| configFilePath | <code>String</code> | 

<a name="configController.restoreConfigFile"></a>

### configController.restoreConfigFile(configFilePath)
Recibe un string con el path del archivo de configuracion, este busca el archivo .old y lo reestablece

**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>\*</code> | 

<a name="configController.restoreDefaultConfigFile"></a>

### configController.restoreDefaultConfigFile(configFilePath)
**Kind**: static method of [<code>configController</code>](#configController)  

| Param | Type |
| --- | --- |
| configFilePath | <code>\*</code> | 

<a name="constanciasController"></a>

## constanciasController
**Kind**: global class  

* [constanciasController](#constanciasController)
    * [.createConstancia(configFile, ncontrol, nombre, carrera, actividades)](#constanciasController.createConstancia)
    * [.havesConstacia(ncontrol)](#constanciasController.havesConstacia) ⇒ <code>Boolean</code>
    * [.genPDF(configFile, ncontrol, savePath)](#constanciasController.genPDF)
    * [.getConstancia(ncontrol)](#constanciasController.getConstancia) ⇒ <code>Object</code>

<a name="constanciasController.createConstancia"></a>

### constanciasController.createConstancia(configFile, ncontrol, nombre, carrera, actividades)
**Kind**: static method of [<code>constanciasController</code>](#constanciasController)  

| Param | Type |
| --- | --- |
| configFile | <code>\*</code> | 
| ncontrol | <code>\*</code> | 
| nombre | <code>\*</code> | 
| carrera | <code>\*</code> | 
| actividades | <code>\*</code> | 

<a name="constanciasController.havesConstacia"></a>

### constanciasController.havesConstacia(ncontrol) ⇒ <code>Boolean</code>
**Kind**: static method of [<code>constanciasController</code>](#constanciasController)  

| Param | Type |
| --- | --- |
| ncontrol | <code>\*</code> | 

<a name="constanciasController.genPDF"></a>

### constanciasController.genPDF(configFile, ncontrol, savePath)
**Kind**: static method of [<code>constanciasController</code>](#constanciasController)  

| Param | Type |
| --- | --- |
| configFile | <code>\*</code> | 
| ncontrol | <code>\*</code> | 
| savePath | <code>\*</code> | 

<a name="constanciasController.getConstancia"></a>

### constanciasController.getConstancia(ncontrol) ⇒ <code>Object</code>
**Kind**: static method of [<code>constanciasController</code>](#constanciasController)  

| Param | Type |
| --- | --- |
| ncontrol | <code>\*</code> | 