let ipcRenderer=require('electron').ipcRenderer

    ipcRenderer.send('obtener-configuracion-asincrono');
    ipcRenderer.on('r-obtener-configuracion-asincrono',(event,args)=>{
        document.getElementById('fac').value=args.con_folio;
        document.getElementById('fan').value=args.con_frase_anual;
        document.getElementById('ses').value=args.con_jefe_servicios_escolares;
        document.getElementById('con').value=args.con_contenido;
        document.getElementById('fpa-preview').src=args.con_fondo;
        document.getElementById('cab-preview').src=args.con_cabecera;
        document.getElementById('ppa-preview').src=args.con_pie;
    })
    function guardar(){
        let campos={
            con_folio:document.getElementById('fac').value,
            con_frase_anual: document.getElementById('fan').value,
            con_jefe_servicios_escolares: document.getElementById('ses').value,
            con_contenido:document.getElementById('con').value,
            con_fondo:document.getElementById('fpa-preview').src,
            con_cabecera:document.getElementById('cab-preview').src,
            con_pie:document.getElementById('ppa-preview').src
        }
        ipcRenderer.send('guardar-configuracion-asincrono',campos);
    }

    ipcRenderer.on('r-guardar-configuracion-asincrono',(event,args)=>{
        alert('Configuracion Guardada con exito!');
    })

    function getDataUrl (imgUrl,callback) {

        let image=new Image();
        image.src=imgUrl;
        image.onload=()=>{
            var canvas = document.createElement('canvas')
            var ctx = canvas.getContext('2d')

            canvas.width = image.width
            canvas.height = image.height
            ctx.drawImage(image, 0, 0)
            callback(canvas.toDataURL())    
        }
        
    }
    function preview()
    {
        let campos={
            con_folio:document.getElementById('fac').value,
            con_frase_anual:document.getElementById('fan').value,
            con_jefe_servicios_escolares:document.getElementById('ses').value,
            con_contenido:document.getElementById('con').value,
            con_fondo:document.getElementById('fpa-preview').src,
            con_cabecera:document.getElementById('cab-preview').src,
            con_pie:document.getElementById('ppa-preview').src
        }
        ipcRenderer.send('preview-configuracion-asincrono',campos);
    }
    // Camio de imagen
    function cambio(id)
    {
        let preview=document.getElementById(id+"-preview");
        let filetag=document.getElementById(id);
        if (filetag.files.length>0)
        {
            let file=filetag.files[0];
            getDataUrl(file.path,(url)=>
            {
                preview.src=url;
            })
        }
    }
    

    