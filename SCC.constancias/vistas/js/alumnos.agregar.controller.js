
let ipcRenderer=require('electron').ipcRenderer


// rellenar carreras con base de datos
ipcRenderer.send('obtener-carreras-asincrono',null);
ipcRenderer.on('r-obtener-carreras-asincrono',(event,args)=>{
    let select=document.getElementById('ca');
    args.forEach(element => {
        select.innerHTML+=`<option value="${element.car_id}">${element.car_nombre}</option>`;
    });
})

function Agregar()
{
    let target=document.getElementById('target');
    let tr=document.createElement('tr');
    tr.classList.toggle('actividad');
    tr.innerHTML=`
    <td>
        <textarea class="form-control"   placeholder="EJ. Concurso ENEIT 2019" rows="2" ></textarea>
    </td>
    <td>
        <select class="form-control">
            <option value="" selected disabled hidden >--Seleccionar Periodo--</option>
            <option value="1">Enero-Junio</option>
            <option value="2">Agosto-Diciembre</option>
        </select>
    </td>
    <td>
        <input type="number" class="form-control" placeholder="EJ. 2019">
    </td>
    <td>
        <input type="number" class="form-control" value="1">
    </td>
    <td><button class="btn btn-outline-danger" onclick="deleteSelf(this)">Borrar</button></td>
    `
    target.appendChild(tr);
}
                    
function deleteSelf(e) 
{ 
    let tr=e.parentElement.parentElement
    
    tr.parentElement.removeChild(tr);
}

function getInfo()
{
    //Obtener informacion del formulario
    let renglones=document.getElementsByClassName('actividad')
    let actividades=[]
    for (let i = 0; i < renglones.length; i++) {
        const renglon = renglones[i];
        //
        let columnas=renglon.children;
        actividades.push({
                nombre:columnas[0].children[0].value,
                periodo:columnas[1].children[0].value,
                anio:columnas[2].children[0].value,
                creditos:columnas[3].children[0].value
            })
    }
    return {
        alumno:{
            ncontrol:document.getElementById('ndc').value,
            nombre:document.getElementById('nc').value,
            carrera:document.getElementById('ca').value,
        },
        actividades:actividades
    }
}

function generarConstancia()
{
    let constancia=getInfo();
    ipcRenderer.send('crear-alumno-asincrono',constancia)
}

ipcRenderer.on('r-crear-alumno-asincrono',(event,args)=>{
    ;
    window.location='alumnos.html'
})


ipcRenderer.on('r-leer-alumnos-excel-asincrono',(event,args)=>{
    ;
});


function leerExcel(e)
{
    ;
    ipcRenderer.send('leer-alumnos-excel-asincrono',e.files[0].path);
    document.getElementById('progreso').style.display="block";
}

let lim=0;
let paso=0;
ipcRenderer.on('r-progreso',(event,args)=>{
    
    document.getElementById('pf').style.width=`${args/paso}%`;
    document.getElementById('pf').innerText=`${args/paso}%`;
    if((args/paso)>=100)
        document.getElementById('pf').innerText="Listo!"
});

ipcRenderer.on('r-progreso-set-titulo',(event,args)=>{
    document.getElementById('titulo').innerText=`${args}`;
});
ipcRenderer.on('r-progreso-set-lim',(event,args)=>{
    lim=args;
    paso=lim/100;
    ;
});
