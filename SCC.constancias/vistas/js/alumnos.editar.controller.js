let ipcRenderer=require('electron').ipcRenderer

;

ipcRenderer.send('obtener-carreras-asincrono',null);
ipcRenderer.on('r-obtener-carreras-asincrono',(event,args)=>{
    let select=document.getElementById('ca');
    args.forEach(element => {
        select.innerHTML+=`<option value="${element.car_id}">${element.car_nombre}</option>`;
    });
})

// Obtener la constancia deseada
ipcRenderer.send('obtener-alumno-asincrono',window.localStorage.getItem('edicion'));

ipcRenderer.on('r-obtener-alumno-asincrono',(event,args)=>{
    //;
    let alu=args.alumno;
    document.getElementById('ndc').value=alu.alu_ncontrol;
    document.getElementById('nc').value=alu.alu_nombre;
    document.getElementById('ca').value=alu.car_id;
    args.actividades.forEach(element => {
        let target=document.getElementById('target');
        let tr=document.createElement('tr');
        tr.classList.toggle('actividad');
        tr.innerHTML=`
        <td>
            <textarea class="form-control"   placeholder="EJ. Concurso ENEIT 2019" rows="2" >${element.act_nombre}</textarea>
        </td>
        <td>
            <select class="form-control" >
                <option value="1" ${element.per_id==1?'selected': ''}>Enero-Junio</option>
                <option value="2" ${element.per_id==2?'selected': ''}>Agosto-Diciembre</option>
            </select>
        </td>
        <td>
            <input type="number" class="form-control" placeholder="EJ. 2019" value="${element.act_anio}">
        </td>
        <td>
            <input type="number" class="form-control" value="${element.act_creditos}">
        </td>
        <td><button class="btn btn-outline-danger" onclick="deleteSelf(this)">Borrar</button></td>
        `;
        target.appendChild(tr);
    });
    
    
})
// LLenar los campos con la constancia


function Agregar()
{
    let target=document.getElementById('target');
    let tr=document.createElement('tr');
    tr.classList.toggle('actividad');
    tr.innerHTML=`
    <td>
        <textarea class="form-control"   placeholder="EJ. Concurso ENEIT 2019" rows="2" ></textarea>
    </td>
    <td>
        <select class="form-control">
            <option value="" selected disabled hidden >--Seleccionar Periodo--</option>
            <option value="1">Enero-Junio</option>
            <option value="2">Agosto-Diciembre</option>
        </select>
    </td>
    <td style="width:5em">
        <input type="number" class="form-control"  placeholder="EJ. 2019">
    </td>
    <td >
        <input type="number" class="form-control"  value="1">
    </td>
    <td><button class="btn btn-outline-danger" onclick="deleteSelf(this)">Borrar</button></td>
    `
    target.appendChild(tr);
}
                    
function deleteSelf(e) 
{ 
    let tr=e.parentElement.parentElement
    
    tr.parentElement.removeChild(tr);
}


function getInfo()
{
    //Obtener informacion del formulario
    let renglones=document.getElementsByClassName('actividad')
    let actividades=[]
    for (let i = 0; i < renglones.length; i++) {
        const renglon = renglones[i];
        //
        let columnas=renglon.children;
        actividades.push({
                nombre:columnas[0].children[0].value,
                periodo:columnas[1].children[0].value,
                anio:columnas[2].children[0].value,
                creditos:columnas[3].children[0].value
            })
    }
    return {
        alumno:{
            ncontrol:document.getElementById('ndc').value,
            nombre:document.getElementById('nc').value,
            carrera:document.getElementById('ca').value,
        },
        actividades:actividades
    }
}

function guardarConstancia()
{
    let constancia=getInfo();
    ipcRenderer.send('editar-alumno-asincrono',constancia)
}

ipcRenderer.on('r-editar-alumno-asincrono',(event,args)=>{
    ;
    window.location='alumnos.html'
})
