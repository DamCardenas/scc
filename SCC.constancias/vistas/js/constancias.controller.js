let ipcRenderer=require('electron').ipcRenderer


function clearTable()
{
    document.getElementById('target').innerHTML="";
}

function fillTable(array)
{
    let elem=document.getElementById('target');
    array.forEach(element => {
        elem.innerHTML+=`<tr><td >${element.cst_id}</td><td>${element.alu_ncontrol}</td><td>
        <a href="#" onclick="imprimir('${element.alu_ncontrol}')" class="badge  badge-info">Imprimir <i class="fas fa-print" style="font-size: 1.5em;"></i></a>
        </td></tr>`
    });
}

ipcRenderer.send('obtener-constancias-asincrono',null);

ipcRenderer.on('r-obtener-constancias-asincrono',(event,args)=>{
    clearTable();
    fillTable(args);
})


ipcRenderer.on('r-buscar-constancias-asincrono',(event,args)=>{
    clearTable();
    fillTable(args);
});

function buscar(e)
{
    //;
    ipcRenderer.send('buscar-constancias-asincrono',e.value);
}

function imprimir(event)
{
    ipcRenderer.send('imprimir-constancia-asincrono',event);
}