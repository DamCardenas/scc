let ipcRenderer=require('electron').ipcRenderer

ipcRenderer.send('obtener-paginacion-asincrona',null)
ipcRenderer.on('r-obtener-paginacion-asincrona',(event,args)=>{
    ;
})



function clearTable()
{
    document.getElementById('target').innerHTML="";
}

function fillTable(array)
{
    let elem=document.getElementById('target');
    array.forEach(element => {
        elem.innerHTML+=`<tr><td >${element.alu_ncontrol}</td><td>${element.alu_nombre}</td><td>
        <a href="#" onclick="edit('${element.alu_ncontrol}')" class="badge  badge-primary">Editar <i class="far fa-edit" style="font-size: 1.5em;"></i></a> 
        <a href="#" onclick="preConstancia('${element.alu_ncontrol}')" class="badge  badge-info">Constancia <i class="fas fa-file-invoice" style="font-size: 1.5em;"></i></a>
        <!-- <a href="#" onclick="preEliminacion('${element.alu_ncontrol}')" class="badge  badge-danger">Eliminar <i class="fas fa-trash" style="font-size: 1.5em;"></i></a> -->
        </td></tr>`
    });
}

ipcRenderer.send('obtener-alumnos-asincrono',0);

ipcRenderer.on('r-obtener-alumnos-asincrono',(event,args)=>{
    clearTable();
    fillTable(args);
})

function edit(event)
{
    // Adquirir informacion
    window.localStorage.setItem('edicion',event)
    window.location="alumnos.editar.html"
}


function preConstancia(event)
{
    // Obtener info del alumno
    ipcRenderer.send('preConstancia-alumno',event);
}

ipcRenderer.on('r-preConstancia-alumno',(event,args)=>{
    //rellenar info
    $("#c-alu-nombre").text(args.nombre);
    // establecer tabla con checkbox
    document.getElementById('c-actividades').innerHTML=""
    for (let i = 0; i < args.actividades.length; i++) {
        const element = args.actividades[i];
        document.getElementById('c-actividades').innerHTML+=`
        <tr class="actividad">
            <td>
                <input type="checkbox" class="form-check-input" name="actividades" value="${element.act_id}">
            </td>
            <td>
                ${element.act_nombre}
            </td>
            <td>
                <input type="number" class="form-control" value="${element.act_creditos}" >
            </td>
        </tr>`;
    }
    // Añadir metodo para generacion de constancias
    $("#crear-constancia").off();
    $("#crear-constancia").on("click",()=>{
        // Obtener arreglo de claves y valores
        let actividades=[];
        let objs=document.getElementsByClassName('actividad');
        let creditos=0.0;
        for (let i = 0; i < objs.length; i++) {
            const element = objs[i];
            if(element.children[0].children[0].checked)
            {
                actividades.push({
                    act_id:element.children[0].children[0].value,
                    act_creditos:element.children[2].children[0].value
                })
                creditos+=parseFloat(element.children[2].children[0].value)
            }
        }
        ;
        if(creditos>=5)
            ipcRenderer.send('crear-constancia-asincrono',{
                alu_ncontrol:args.ncontrol,
                actividades:actividades,
                car_id:args.carrera
            });
        else
            alert('Revise que la suma de las actividades\nseleccionadas sea mayor o igual que 5...');
    })
    // Mostrar modal
    $('#constancias').modal('show');
})
ipcRenderer.on('r-crear-constancia-asincrono',(event,args)=>{
    $('#constancias').modal('hide');
    ipcRenderer.send('obtener-alumnos-asincrono',0);
    ;
    if(args.changes>=1)
        alert("Se creo la constancia de manera exitosa!");
});

function preEliminacion(event)
{
    // Obtener info del alumno
    ipcRenderer.send('preEliminar-alumno',event);
}
ipcRenderer.on('r-preEliminar-alumno',(event,args)=>{
    // Rellenar info del modal
    $("#e-alu-nombre").text(args.nombre);
    $("#e-alu-ncontrol").text(args.ncontrol);
    $("#e-alu-carrera").text(args.carrera);
    document.getElementById('e-actividades').innerHTML=""
    for (let i = 0; i < args.actividades.length; i++) {
        const element = args.actividades[i];
        document.getElementById('e-actividades').innerHTML+=`
        <tr>
            <td>
                ${element.act_nombre}
            </td>
            <td>
                ${element.act_creditos}
            </td>
        </tr>`;
    }
    // Añadir metodo de eliminacion
    $("#eliminar-alumno").off();
    $("#eliminar-alumno").on("click",()=>{
        ipcRenderer.send('eliminar-alumno-asincrono',args.ncontrol);
    })
    // Mostrar modal
    $('#delete').modal('show');
})

ipcRenderer.on('r-eliminar-alumno-asincrono',(event,args)=>{
    $('#delete').modal('hide')
    ipcRenderer.send('obtener-alumnos-asincrono',0);
})

ipcRenderer.on('r-buscar-alumnos-asincrono',(event,args)=>{
    clearTable();
    fillTable(args);
});

function buscar(e)
{
    //;
    ipcRenderer.send('buscar-alumnos-asincrono',e.value);
}